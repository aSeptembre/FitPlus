# Membres du groupe

Les membres du groupe qui ont réalisé ce projet sont :
* SEPTEMBRE Alexandre
* MARGAIL Maurin

# Projet

Le projet réalisé est une application de musculation avec un menu qui a 4 onglets

## dashboard

La première page de l'application est une sorte de dashboard avec des cardviews personnalisés

## exercices

La deuxième page est une liste d'élèments représentant des exercices de fitness

## profil

La troisième page est une page de profil où les données sont sauvegardées dans les fichiers des SharedPreferences. Les données sont : le poid (Integer), la taille en cm (Integer), et la difficultés des exercices (Integer (id du radioButton))

## météo

La dernière page affiche la météo à Paris au moment où l'application est lancé en faisant un appel à l'API openweathermap