package fr.septembre.fitplus.ui.main

import fr.septembre.fitplus.R

data class Exercice (var name: String = "Toto", var image: Int = R.mipmap.abdominaux)