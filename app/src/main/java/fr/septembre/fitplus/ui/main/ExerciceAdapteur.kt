package fr.septembre.fitplus.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import fr.septembre.fitplus.R
import kotlinx.android.synthetic.main.exercice_list.view.*

class ExerciceAdapteur (val items: Array<Exercice>): RecyclerView.Adapter<ExerciceAdapteur.ViewHolder>() {
        class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
            fun bindAndVersion(exercice: Exercice) {
                with(exercice) {
                    itemView.findViewById<TextView>(R.id.exerciceName).text = "$name"
                    itemView.findViewById<ImageView>(R.id.exerciceImage).setImageResource(image)
                }
            }
        }

        override fun getItemCount(): Int = items.size

        fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
            return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExerciceAdapteur.ViewHolder {
            return ViewHolder(parent.inflate(R.layout.exercice_list))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bindAndVersion(items[position])
        }
}