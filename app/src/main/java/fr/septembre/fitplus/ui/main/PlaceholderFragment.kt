package fr.septembre.fitplus.ui.main

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import fr.septembre.fitplus.R
import okhttp3.*
import java.io.IOException

/**
 * A placeholder fragment containing a simple view.
 */
class PlaceholderFragment : Fragment(){

    private val client = OkHttpClient()
    private val appid: String = "ecf35303120c0748752f056181a06cec"
    private val ville: String = "Paris"
    private val lang = "fr"
    private lateinit var reponse: Meteo

    private lateinit var pageViewModel: PageViewModel
    private lateinit var sharedPreferences: SharedPreferences

    private lateinit var seekbarPoid: NumberPicker
    private lateinit var seekbarTaille: NumberPicker
    private lateinit var radioGroupDiffLevel: RadioGroup

    private var items = Array<Exercice>(10, { Exercice() })

    private var page: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.sharedPreferences = context!!.getSharedPreferences("profil", Context.MODE_PRIVATE)
        appelleApi("api.openweathermap.org/data/2.5/weather", this.ville, this.lang, this.appid)
        if (arguments?.getInt(ARG_SECTION_NUMBER) ?: 1 == 1) {
            pageViewModel = ViewModelProviders.of(this).get(PageViewModel::class.java).apply {
                setIndex(10)
                page = 1
            }
        }
        if (arguments?.getInt(ARG_SECTION_NUMBER) ?: 1 == 2) {
            pageViewModel = ViewModelProviders.of(this).get(PageViewModel::class.java).apply {
                setIndex(20)
                page = 2
            }
        }
        if (arguments?.getInt(ARG_SECTION_NUMBER) ?: 1 == 3) {
            pageViewModel = ViewModelProviders.of(this).get(PageViewModel::class.java).apply {
                setIndex(30)
                page = 3
            }
        }
        if (arguments?.getInt(ARG_SECTION_NUMBER) ?: 1 == 4) {
            pageViewModel = ViewModelProviders.of(this).get(PageViewModel::class.java).apply {
                setIndex(30)
                page = 4
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        var root = inflater.inflate(R.layout.fragment_main, container, false)

        if (page == 1) {
            root = inflater.inflate(R.layout.fragment_resume, container, false)

        }
        if (page == 2) {
            root = inflater.inflate(R.layout.fragment_exercice, container, false)
            seedItems()
            root.findViewById<RecyclerView>(R.id.recyclerView).layoutManager = LinearLayoutManager(context)
            root.findViewById<RecyclerView>(R.id.recyclerView).adapter = ExerciceAdapteur(items)

        }
        if (page == 3) {
            root = inflater.inflate(R.layout.fragment_settings, container, false)

            seekbarPoid = root.findViewById(R.id.seekBar_poid)
            seekbarPoid.minValue = 0
            seekbarPoid.maxValue = 200
            seekbarPoid.value = this.sharedPreferences.getInt("poid", 75)

            seekbarTaille = root.findViewById(R.id.seekBar_taille)
            seekbarTaille.minValue = 100
            seekbarTaille.maxValue = 250
            seekbarTaille.value = this.sharedPreferences.getInt("taille", 175)

            val checkedRadioButtonId: Int = this.sharedPreferences.getInt("checkedRadioButtonId", R.id.radioButton_easy)
            radioGroupDiffLevel = root.findViewById(R.id.radioGroup_diffLevel)
            radioGroupDiffLevel.check(checkedRadioButtonId)

            val buttonSave: Button = root.findViewById(R.id.button_save)
            buttonSave.setOnClickListener {
                doSave()
            }
        }

        if (page == 4) {
            root = inflater.inflate(R.layout.fragment_meteo, container, false)

            root.findViewById<TextView>(R.id.tempApi).text = sharedPreferences.getFloat("temp", 10F).toString()
            root.findViewById<TextView>(R.id.conditionMeteoApi).text = sharedPreferences.getString("condMeteo", "clear")
            root.findViewById<TextView>(R.id.descriptionApi).text = sharedPreferences.getString("description", "ciel dégagé")
        }
        return root
    }

    private fun doSave() {
        val editor: SharedPreferences.Editor = this.sharedPreferences.edit()
        val checkedRadioButtonId: Int = radioGroupDiffLevel.checkedRadioButtonId

        editor.putInt("poid", this.seekbarPoid.value)
        editor.putInt("taille", this.seekbarTaille.value)
        editor.putInt("checkedRadioButtonId", checkedRadioButtonId)

        editor.apply()

        Toast.makeText(context, "Profil Sauvegardé !", Toast.LENGTH_LONG).show()
    }

    private fun appelleApi(url: String, ville: String, lang: String, appid: String) {
        val urlBuild: String = "http://$url?q=$ville&units=metric&lang=$lang&appid=$appid"
        val request = Request.Builder()
            .url(urlBuild)
            .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {}
            override fun onResponse(call: Call, response: Response) {
                reponse = Gson().fromJson(response.body()?.string(), Meteo::class.java)
                println(reponse)
                val editor: SharedPreferences.Editor = sharedPreferences.edit()

                editor.putFloat("temp", reponse.main.temp)
                editor.putString("condMeteo", reponse.weather[0].main)
                editor.putString("description", reponse.weather[0].description)

                editor.apply()
            }
        })
    }
    private fun seedItems() {
        val nameArray = resources.getStringArray(R.array.exo)
        val imageArray = mutableListOf<Int>()
        imageArray.add(R.mipmap.abdominaux)
        imageArray.add(R.mipmap.dorsaux)
        imageArray.add(R.mipmap.biceps)
        imageArray.add(R.mipmap.mollets)
        imageArray.add(R.mipmap.cardio)
        imageArray.add(R.mipmap.pectoraux)
        imageArray.add(R.mipmap.deltoides)
        imageArray.add(R.mipmap.ischiosjambiers)
        imageArray.add(R.mipmap.quadriceps)
        imageArray.add(R.mipmap.triceps)
        for (k in 0..9) {
            items[k] = Exercice(nameArray[k], imageArray[k])
        }
    }

    private fun updateObjectList(adapter: ExerciceAdapteur) {
        adapter.notifyDataSetChanged()
    }
    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): PlaceholderFragment {
            return PlaceholderFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}