package fr.septembre.fitplus.ui.main

import java.lang.reflect.Array
import java.util.ArrayList

data class Coordonnees(var lon: Float, var lat: Float)

data class Weather(var id: Int, var main: String, var description: String, var icon: String)

data class Main(var temp: Float, var feels_like: Float, var temp_min: Float, var temp_max: Float, var pressure: Float, var humidity: Float)

data class Wind (var speed: Float, var deg: Int)

data class Clouds(var all: Int)

data class Sys(var type: Int, var id: Int, var country: String, var sunrise: Long, var sunset: Long)

data class Meteo(var coord: Coordonnees, var weather: List<Weather>, var base: String, var main: Main, var visibility: Int, var wind: Wind, var clouds: Clouds, var dt: Long, var sys: Sys, var timezone: Int, var id: Long, var name: String, var cod: Int)